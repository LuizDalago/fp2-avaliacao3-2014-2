package utfpr.ct.dainf.if62c.avaliacao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * IF62C Fundamentos de Programação 2
 * Avaliação parcial.
 * @author 
 */
public class ProcessaLancamentos {
    private BufferedReader reader;

    public ProcessaLancamentos(File arquivo) throws FileNotFoundException {
        reader = new BufferedReader(new FileReader(arquivo));
    }

    public ProcessaLancamentos(String path) throws FileNotFoundException {
        this(new File(path));
    }
    
    private String getNextLine() throws IOException {
        return reader.readLine();
    }
    
    private Lancamento processaLinha(String linha) {
        String data = linha.substring(6,14);
        Date d = new GregorianCalendar(Integer.valueOf(data.substring(0,4)),Integer.valueOf(data.substring(4,6))-1,Integer.valueOf(data.substring(6,8))).getTime();
        return new Lancamento(Integer.valueOf(linha.substring(0, 6)),d,linha.substring(14, 74),Double.valueOf(linha.substring(74,86))/100);
    }
    
    private Lancamento getNextLancamento() throws IOException {
        String s = getNextLine();
        if(s == null || s.isEmpty()){
            return null;
        }
        return processaLinha(s);
    }
    
    public List<Lancamento> getLancamentos() throws IOException {
        try{
            List<Lancamento> lista = new ArrayList<>();
            for(Lancamento l = getNextLancamento(); l != null; l = getNextLancamento()){
                lista.add(l);
            }
            lista.sort(new LancamentoComparator());
            return lista;
        }finally{
            reader.close();
        }
    }
    
}
