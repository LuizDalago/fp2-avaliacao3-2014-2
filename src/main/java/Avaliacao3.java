
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;
import utfpr.ct.dainf.if62c.avaliacao.Lancamento;
import utfpr.ct.dainf.if62c.avaliacao.ProcessaLancamentos;

/**
 * IF62C Fundamentos de Programação 2
 * Avaliação parcial.
 * @author 
 */
public class Avaliacao3 {
 
    public static void main(String[] args) throws FileNotFoundException, IOException {
       Scanner s = new Scanner(System.in);
       List<Lancamento> lancamentos = new  ProcessaLancamentos(s.nextLine()).getLancamentos();
       int n= -1;
       
       do{
           try{
                n = Integer.valueOf(s.nextLine());
                exibeLancamentosConta(lancamentos, n);
                if(lancamentos.indexOf(new Lancamento(n, null, null, Double.NaN)) == -1){
                    System.out.println("Conta inexistente");               
                }
           }catch(NumberFormatException ex){
                System.out.println("Por favor, informe um valor numérico");               
           }
       }while(n != 0);
    }
    
    public static void exibeLancamentosConta(List<Lancamento> lancamentos, Integer conta) {
        int i = lancamentos.indexOf(new Lancamento(conta, null, null, Double.NaN));
        if(i == -1){
            return ;
        }
        for(;i < lancamentos.size();i++){
            if(lancamentos.get(i).getConta().equals(conta)){
                System.out.println(lancamentos.get(i));
            }else{
                break;
            }  
           
        }
    }
 
}